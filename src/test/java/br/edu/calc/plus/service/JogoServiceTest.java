/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.service;

import br.edu.calc.plus.domain.EOperator;
import br.edu.calc.plus.domain.Jogo;
import br.edu.calc.plus.repo.JogoRepo;
import br.edu.calc.plus.util.Util;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author saulo
 */
@ExtendWith(SpringExtension.class)        
@ActiveProfiles("test")
public class JogoServiceTest {
    
    @MockBean
    private JogoRepo jDao;
    
    @MockBean
    private Random rand;

    @InjectMocks
    JogoService jServ;
    
    public JogoServiceTest() {
    }
    
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testPegarErros() {
        //cenario
        String Esperado = Util.formatEmK(2);
        Mockito.when(jDao.getAllErros()).thenReturn((long)2);
        
        //execução
        String Obtido = jServ.getAllErros();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testPegarAcertos() {
        //cenario
        int Esperado = 3;
        Mockito.when(jDao.getAllAcertos()).thenReturn((long)3);
        
        //execução
        long Obtido = jServ.getAllAcertos();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testPegarAcertosUltimos10Dias() {
        //cenario
        long[] Esperado = {10,10,10,10,10,10,10,10,10,10};
        
        Mockito.when(jDao.getAcertosDia(Mockito.any(),Mockito.any())).thenReturn((long)10);
        
        //execução
        long[] Obtido = jServ.getAcertosUltimos10Dias();
        
        //verificação
       assertArrayEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testPegarErrosUltimos10Dias() {
        //cenario
        long[] Esperado = {5,5,5,5,5,5,5,5,5,5};
        
        Mockito.when(jDao.getErrosDia(Mockito.any(),Mockito.any())).thenReturn((long)5);
        
        //execução
        long[] Obtido = jServ.getErrosUltimos10Dias();
        
        //verificação
       assertArrayEquals(Esperado, Obtido);
        
    }
    
    // Ate 10 acertos
    
    @Test
    public void testBonusInicialOperacaoSomaAte10Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(3.7)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)4);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.soma, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testBonusInicialOperacaoSubtracaoAte10Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(3.8)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)4);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.subtracao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }

    @Test
    public void testBonusInicialOperacaoMultiplacacaoAte10Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(4.1)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)4);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.multiplicacao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testBonusInicialOperacaoDivisaoAte10Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(4.5)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)4);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.divisao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    // Até 50 acertos
    
    @Test
    public void testBonusInicialOperacaoSomaAte50Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(4.07)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)20);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.soma, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testBonusInicialOperacaoSubtracaoAte50Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(4.18)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)20);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.subtracao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }

    @Test
    public void testBonusInicialOperacaoMultiplacacaoAte50Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(4.51)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)20);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.multiplicacao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testBonusInicialOperacaoDivisaoAte50Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(4.95)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)20);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.divisao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    // Até 100 acertos
    
    @Test
    public void testBonusInicialOperacaoSomaAte100Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(4.44)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)60);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.soma, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testBonusInicialOperacaoSubtracaoAte100Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(4.56)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)60);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.subtracao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }

    @Test
    public void testBonusInicialOperacaoMultiplacacaoAte100Acertos() {
        //cenario
        //4.92 Valor arredondado
        //BigDecimal Esperado = new BigDecimal(String.valueOf(4.92)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        //4.41 valor normal
        BigDecimal Esperado = new BigDecimal(String.valueOf(4.91)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)60);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.multiplicacao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testBonusInicialOperacaoDivisaoAte100Acertos() {
        //cenario
        //5.4 valor arredondado
        //BigDecimal Esperado = new BigDecimal(String.valueOf(5.4)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        BigDecimal Esperado = new BigDecimal(String.valueOf(5.39)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)60);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.divisao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    // Até 150 acertos
    
    @Test
    public void testBonusInicialOperacaoSomaAte150Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(4.81)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)110);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.soma, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testBonusInicialOperacaoSubtracaoAte150Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(4.94)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)110);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.subtracao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }

    @Test
    public void testBonusInicialOperacaoMultiplacacaoAte150Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(5.33)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)110);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.multiplicacao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testBonusInicialOperacaoDivisaoAte150Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(5.85)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)110);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.divisao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    // Até 200 acertos
    
    @Test
    public void testBonusInicialOperacaoSomaAte200Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(5.55)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)160);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.soma, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testBonusInicialOperacaoSubtracaoAte200Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(5.7)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)160);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.subtracao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }

    @Test
    public void testBonusInicialOperacaoMultiplacacaoAte200Acertos() {
        //cenario
        //6.15 valor arredondado
        //BigDecimal Esperado = new BigDecimal(String.valueOf(6.15)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        BigDecimal Esperado = new BigDecimal(String.valueOf(6.14)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)160);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.multiplicacao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testBonusInicialOperacaoDivisaoAte200Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(6.75)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)160);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.divisao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    // Mais 200 acertos
    
    @Test
    public void testBonusInicialOperacaoSomaMais200Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(6.66)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)210);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.soma, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testBonusInicialOperacaoSubtracaoMais200Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(6.84)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)210);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.subtracao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }

    @Test
    public void testBonusInicialOperacaoMultiplacacaoMais200Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(7.38)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)210);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.multiplicacao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testBonusInicialOperacaoDivisaoMais200Acertos() {
        //cenario
        BigDecimal Esperado = new BigDecimal(String.valueOf(8.1)).setScale(2, BigDecimal.ROUND_FLOOR); 
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)210);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        //execução
        BigDecimal Obtido = jServ.bonusInicial(EOperator.divisao, 1);
        
       //verificação
       assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testCriarJogoAleatorioSoma() {
        //cenario
        Jogo Esperado = new Jogo(null, 8, 2, EOperator.soma, 10, -1111, 7.38);
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)210);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        Mockito.when(rand.nextInt(50)).thenReturn(8);
        Mockito.when(rand.nextInt(4)).thenReturn(0);
        
        //execução
        List<Jogo> Obtido = jServ.criarJogosAleatorio(1, 1);
        
       //verificação
        assertEquals(Esperado, Obtido.get(0));
    }
    
    @Test
    public void testCriarJogoAleatorioSubtracao() {
        //cenario
        Jogo Esperado = new Jogo(null, 8, 2, EOperator.subtracao, 6, -1111, 7.38);
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)210);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        Mockito.when(rand.nextInt(50)).thenReturn(8);
        Mockito.when(rand.nextInt(4)).thenReturn(1);
        
        //execução
        List<Jogo> Obtido = jServ.criarJogosAleatorio(1, 1);
        
       //verificação
        assertEquals(Esperado, Obtido.get(0));
    }
    
    @Test
    public void testCriarJogoAleatorioMultiplicacao() {
        //cenario
        Jogo Esperado = new Jogo(null, 8, 2, EOperator.multiplicacao, 64, -1111, 7.38);
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)210);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        Mockito.when(rand.nextInt(50)).thenReturn(8);
        Mockito.when(rand.nextInt(4)).thenReturn(2);
        
        //execução
        List<Jogo> Obtido = jServ.criarJogosAleatorio(1, 1);
        
       //verificação
        assertEquals(Esperado, Obtido.get(0));
    }
    
    @Test
    public void testCriarJogoAleatorioDivisao() {
        //cenario
        Jogo Esperado = new Jogo(null, 8, 2, EOperator.divisao, 4, -1111, 7.38);
        
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn((long)210);
        Mockito.when(rand.nextDouble()).thenReturn(0.4);
        
        Mockito.when(rand.nextInt(50)).thenReturn(8);
        Mockito.when(rand.nextInt(4)).thenReturn(3);
        
        //execução
        List<Jogo> Obtido = jServ.criarJogosAleatorio(1, 1);
        
       //verificação
        assertEquals(Esperado, Obtido.get(0));
    }
    
}
