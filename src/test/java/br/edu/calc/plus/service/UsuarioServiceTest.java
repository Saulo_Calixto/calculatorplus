/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.service;

import br.edu.calc.plus.domain.Usuario;
import br.edu.calc.plus.domain.dto.UserDTO;
import br.edu.calc.plus.repo.UsuarioRepo;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author saulo
 */
@ExtendWith(SpringExtension.class)        
@ActiveProfiles("test")
public class UsuarioServiceTest {
    
    @MockBean
    private UsuarioRepo uDao;

    @InjectMocks
    UsuarioService User;
    
    public UsuarioServiceTest() {
    }
    
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testContarUsuarios() {
        
        //preparar
        long qtdUser = 4;
        Mockito.when(uDao.count()).thenReturn(qtdUser);
        
        //executar
        long NumUsuarios = User.getCountUsers();
        
        //validar
        assertEquals(qtdUser, NumUsuarios);
        
    }
    
    @Test
    public void testContarUsuariosSemTerUsuariosSalvos() {
        //cenario
        long Esperado = 0;
        Mockito.when(uDao.count()).thenReturn(Esperado);
        
        //execução
        long Obtido = User.getCountUsers();
        
        //validar
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testSalvarUsuario() {
        
        //preparar
        UserDTO userdto = new UserDTO("Gabriel", "gabi1", "Gabriel#1", "Gabi@Gabi", "cidade1", LocalDate.of(2000, 01, 01));
        Usuario user = new Usuario(1, "Gabriel", "gabi1", "Gabi@Gabi", "Gabriel#1", "cidade1", LocalDate.of(2000, 01, 01));
        
        Mockito.when(uDao.save(user)).thenReturn(user);
        
        try {
            //execução
            User.save(userdto);
            assertTrue(true);//certo
        } catch (Exception e) {
            fail("Erro - Não foi possivel salvar o usuário!!!");//errado
        }
        
    }
    
    @Test
    public void testSalvarUsuarioNulo() {
        
        //preparar
        UserDTO userdto = null;
        Usuario user = null;
        
        Mockito.when(uDao.save(user)).thenReturn(user);
        
        try {
            //execução
            User.save(userdto);
            //errado
            fail("Erro - Não foi possivel salvar o usuário!!!");
        } catch (Exception e) {
            //certo
            assertTrue(true);
            
        }
        
    }
    
}
