/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.service;

import br.edu.calc.plus.domain.EOperator;
import br.edu.calc.plus.domain.Jogo;
import br.edu.calc.plus.domain.Partida;
import br.edu.calc.plus.domain.Usuario;
import br.edu.calc.plus.domain.dto.JogoListDTO;
import br.edu.calc.plus.domain.dto.RankingDTO;
import br.edu.calc.plus.repo.JogoRepo;
import br.edu.calc.plus.repo.PartidaRepo;
import br.edu.calc.plus.repo.UsuarioRepo;
import br.edu.calc.plus.util.Util;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.bytebuddy.implementation.bytecode.Throw;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author saulo
 */
@ExtendWith(SpringExtension.class)        
@ActiveProfiles("test")
public class PartidaServiceTest {
    
    @MockBean
    private PartidaRepo pDao;

    @MockBean
    private JogoRepo jDao;

    @MockBean
    private UsuarioRepo uDao;

    @MockBean
    private JogoService jServ;

    @InjectMocks
    private PartidaService pServ;
    
    public PartidaServiceTest() {
    }
    
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testPegarPremioUsuario() {
        //cenario 
        String Esperado = Util.formatarMoeda((double)20);
        
        Mockito.when(pDao.getAllBonus()).thenReturn((double)20);
        
        //execução
        String Obtido = pServ.getPremioUsers();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testPegarPremioUsuarioErro() {
        //cenario 
        String Esperado = Util.formatarMoeda((double)0);
        
        Mockito.when(pDao.getAllBonus()).thenReturn(null);
        
        //execução
        String Obtido = pServ.getPremioUsers();
        
        //verificação
        assertEquals(Esperado, Obtido);
           
    }
    
    @Test
    public void testPegarMeusJogos() {
        //cenario 
        Usuario user = new Usuario(1, "zezin", "Zezin1", "Ze@Ze", "Zezin#1", "Cidade1", LocalDate.of(2021, 02, 02));
        
        Jogo j1 = new Jogo(1, 2, 2, EOperator.soma, 4, 4, 10);
        Jogo j2 = new Jogo(2, 8, 2, EOperator.subtracao, 6, 6, 10);
        Jogo j3 = new Jogo(3, 1, 1, EOperator.soma, 2, 3, 10);
        
        List<Jogo> jl = new ArrayList<>();
        jl.add(j1);
        jl.add(j2);
        jl.add(j3);
        
        Partida p1 = new Partida(1, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        p1.setUsuario(user);
        p1.setJogoList(jl);
        Partida p2 = new Partida(2, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        p2.setUsuario(user);
        p2.setJogoList(jl);

        List<Partida> ls = new ArrayList<>();
        ls.add(p1);
        ls.add(p2);
        
        List<JogoListDTO> Esperado = new ArrayList<>();
        
        Esperado.add(new JogoListDTO(p1.getId(), p1.getData(), p1.getBonificacao(), p1.getAcertos(), p1.getErros(), p1.getTempo()));
        Esperado.add(new JogoListDTO(p2.getId(), p2.getData(), p2.getBonificacao(), p2.getAcertos(), p2.getErros(), p2.getTempo()));

        Mockito.when(pDao.findByUsuarioId(1)).thenReturn(ls);
        
        //execução
        List<JogoListDTO> Obtido = pServ.getMeusJogos(1);
        
        //verificação
        assertEquals(Esperado, Obtido);
    
            
    }
    
    @Test
    public void testUserJaCompetiuHojeCompetiu() {
        //cenario 
        boolean Esperado = true;
        
        Mockito.when(pDao.getUsuarioCompetil(Mockito.any(Integer.class),
                Mockito.any(LocalDateTime.class), Mockito.any(LocalDateTime.class)))
                .thenReturn((long)1);
        
        //execução
        boolean Obtido = pServ.userJaCompetiuHoje(1);
        
        //verificação
        assertEquals(Esperado, Obtido);
    
    }
    
    @Test
    public void testUserJaCompetiuHojeNaoCompetiu() {
        //cenario 
        boolean Esperado = false;
        
        Mockito.when(pDao.getUsuarioCompetil(Mockito.any(Integer.class),
                Mockito.any(LocalDateTime.class), Mockito.any(LocalDateTime.class)))
                .thenReturn((long)0);
        
        //execução
        boolean Obtido = pServ.userJaCompetiuHoje(1);
        
        //verificação
        assertEquals(Esperado, Obtido);
    
    }
    
    @Test
    public void testiniciarPartidaSucess() throws Exception {
        //cenario 
        Usuario user = new Usuario(1, "zezin", "Zezin1", "Ze@Ze", "Zezin#1", "Cidade1", LocalDate.of(2021, 02, 02));
        
        Jogo j1 = new Jogo(1, 2, 2, EOperator.soma, 4, 4, 10);
        Jogo j2 = new Jogo(2, 8, 2, EOperator.subtracao, 6, 6, 10);
        Jogo j3 = new Jogo(3, 1, 1, EOperator.soma, 2, 3, 10);
        
        List<Jogo> jl = new ArrayList<>();
        jl.add(j1);
        jl.add(j2);
        jl.add(j3);
        
        Partida p1 = new Partida(1, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        p1.setUsuario(user);
        p1.setJogoList(jl);
        
        List<Jogo> lJ = new ArrayList<>();
        Jogo aux = new Jogo(null, 8, 2, EOperator.soma, 10, -1111, 7.38);
        aux.setPartida(p1);
        lJ.add(aux);

        Partida Esperado = new Partida(null, LocalDateTime.now(), 0, 0);
	Esperado.setUsuario(user);
        Esperado.setJogoList(lJ);
        
        Mockito.when(uDao.getById(Mockito.any(Integer.class))).thenReturn(user);
        Mockito.when(pDao.save(Mockito.any())).thenReturn(p1);
        Mockito.when(jDao.save(Mockito.any())).thenReturn(j1);
        Mockito.when(jServ.criarJogosAleatorio(Mockito.any(Integer.class), Mockito.any(Integer.class))).thenReturn(lJ);
                
        //execução
        Partida Obtido = pServ.iniciarPartida(1);
        
        //verificação
        assertEquals(Esperado, Obtido);
    
            
    }
    
    @Test
    public void testiniciarPartidaComUsuarioNulo() {
        //cenario 
        Usuario user = new Usuario(1, "zezin", "Zezin1", "Ze@Ze", "Zezin#1", "Cidade1", LocalDate.of(2021, 02, 02));
        
        Jogo j1 = new Jogo(1, 2, 2, EOperator.soma, 4, 4, 10);
        Jogo j2 = new Jogo(2, 8, 2, EOperator.subtracao, 6, 6, 10);
        Jogo j3 = new Jogo(3, 1, 1, EOperator.soma, 2, 3, 10);
        
        List<Jogo> jl = new ArrayList<>();
        jl.add(j1);
        jl.add(j2);
        jl.add(j3);
        
        Partida p1 = new Partida(1, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        p1.setUsuario(user);
        p1.setJogoList(jl);
        
        List<Jogo> lJ = new ArrayList<>();
        Jogo aux = new Jogo(null, 8, 2, EOperator.soma, 10, -1111, 7.38);
        aux.setPartida(p1);
        lJ.add(aux);

        Partida Esperado = new Partida(null, LocalDateTime.now(), 0, 0);
	Esperado.setUsuario(user);
        Esperado.setJogoList(lJ);
        
        Mockito.when(uDao.getById(Mockito.any(Integer.class))).thenReturn(null);
        Mockito.when(pDao.save(Mockito.any())).thenReturn(p1);
        Mockito.when(jDao.save(Mockito.any())).thenReturn(j1);
        Mockito.when(jServ.criarJogosAleatorio(Mockito.any(Integer.class), Mockito.any(Integer.class))).thenReturn(lJ);
                
        Partida Obtido;
        try {
            //execução
            Obtido = pServ.iniciarPartida(1);
            fail("Erro: Criou uma Partida!!!");//Errado
        } catch (Exception ex) {
            //verificação
            assertTrue(true);//certo
        }
        
    }

    
    @Test
    public void testSavePartidaSucessAddBonus() throws Exception {
        //cenario 
        Usuario user = new Usuario(1, "zezin", "Zezin1", "Ze@Ze", "Zezin#1", "Cidade1", LocalDate.of(2021, 02, 02));
        
        Jogo j1 = new Jogo(1, 2, 2, EOperator.soma, 4, 4, 10);
        Jogo j2 = new Jogo(2, 8, 2, EOperator.subtracao, 6, 6, 10);
        Jogo j3 = new Jogo(3, 1, 1, EOperator.soma, 2, 3, 10);
        
        List<Jogo> jl = new ArrayList<>();
        jl.add(j1);
        jl.add(j2);
        jl.add(j3);
        
        Partida p1 = new Partida(1, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        p1.setUsuario(user);
        p1.setJogoList(jl);
        
        List<Jogo> lJ = new ArrayList<>();
        Jogo aux = new Jogo(null, 8, 2, EOperator.soma, 10, -1111, 7.38);
        aux.setPartida(p1);
        lJ.add(aux);
        
        p1.addBonus(jl.get(0).getBonus());
        
        Partida Esperado = p1;
	
        Mockito.when(pDao.findByIdAndUsuarioId(Mockito.any(Integer.class), Mockito.any(Integer.class))).thenReturn(p1);
        Mockito.when(pDao.save(Mockito.any())).thenReturn(p1);
        Mockito.when(jDao.save(Mockito.any())).thenReturn(j1);
        Mockito.when(jServ.criarJogosAleatorio(Mockito.any(Integer.class), Mockito.any(Integer.class))).thenReturn(lJ);
                
        //execução
        Partida Obtido = pServ.savePartida(1, 1, 1, 1, 4);
        
        //verificação
        assertEquals(Esperado, Obtido);
    
            
    }

    @Test
    public void testSavePartidaSucessRemoveBonus() throws Exception {
        //cenario 
        Usuario user = new Usuario(1, "zezin", "Zezin1", "Ze@Ze", "Zezin#1", "Cidade1", LocalDate.of(2021, 02, 02));
        
        Jogo j1 = new Jogo(1, 2, 2, EOperator.soma, 4, 4, 10);
        Jogo j2 = new Jogo(2, 8, 2, EOperator.subtracao, 6, 6, 10);
        Jogo j3 = new Jogo(3, 1, 1, EOperator.soma, 2, 3, 10);
        
        List<Jogo> jl = new ArrayList<>();
        jl.add(j1);
        jl.add(j2);
        jl.add(j3);
        
        Partida p1 = new Partida(1, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        p1.setUsuario(user);
        p1.setJogoList(jl);
        
        List<Jogo> lJ = new ArrayList<>();
        Jogo aux = new Jogo(null, 8, 2, EOperator.soma, 10, -1111, 7.38);
        aux.setPartida(p1);
        lJ.add(aux);
        
        p1.removeBonus(jl.get(0).getBonus() / 2);
        
        Partida Esperado = p1;
	
        Mockito.when(pDao.findByIdAndUsuarioId(Mockito.any(Integer.class), Mockito.any(Integer.class))).thenReturn(p1);
        Mockito.when(pDao.save(Mockito.any())).thenReturn(p1);
        Mockito.when(jDao.save(Mockito.any())).thenReturn(j1);
        Mockito.when(jServ.criarJogosAleatorio(Mockito.any(Integer.class), Mockito.any(Integer.class))).thenReturn(lJ);
                
        //execução
        Partida Obtido = pServ.savePartida(1, 1, 1, 1, 5);
        
        //verificação
        assertEquals(Esperado, Obtido);
    
            
    }

    @Test
    public void testSavePartidaPartidaNula()  {
        //cenario 
        Usuario user = new Usuario(1, "zezin", "Zezin1", "Ze@Ze", "Zezin#1", "Cidade1", LocalDate.of(2021, 02, 02));
        
        Jogo j1 = new Jogo(1, 2, 2, EOperator.soma, 4, 4, 10);
        Jogo j2 = new Jogo(2, 8, 2, EOperator.subtracao, 6, 6, 10);
        Jogo j3 = new Jogo(3, 1, 1, EOperator.soma, 2, 3, 10);
        
        List<Jogo> jl = new ArrayList<>();
        jl.add(j1);
        jl.add(j2);
        jl.add(j3);
        
        Partida p1 = new Partida(1, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        p1.setUsuario(user);
        p1.setJogoList(jl);
        
        List<Jogo> lJ = new ArrayList<>();
        Jogo aux = new Jogo(null, 8, 2, EOperator.soma, 10, -1111, 7.38);
        aux.setPartida(p1);
        lJ.add(aux);
        
        p1.removeBonus(jl.get(0).getBonus() / 2);
        
        Mockito.when(pDao.findByIdAndUsuarioId(Mockito.any(Integer.class), Mockito.any(Integer.class))).thenReturn(null);
        Mockito.when(pDao.save(Mockito.any())).thenReturn(p1);
        Mockito.when(jDao.save(Mockito.any())).thenReturn(j1);
        Mockito.when(jServ.criarJogosAleatorio(Mockito.any(Integer.class), Mockito.any(Integer.class))).thenReturn(lJ);
                
        try {
            //execução
            Partida Obtido = pServ.savePartida(1, 1, 1, 1, 5);
            fail("Erro: Pertida Nula!!!");
        } catch (Exception e) {
            //verificação
            assertTrue(true);
        }
            
    }
    
    @Test
    public void testFinalizaPartidaSemAcertarTudo() throws Exception {
        //cenario 
        Usuario user = new Usuario(1, "zezin", "Zezin1", "Ze@Ze", "Zezin#1", "Cidade1", LocalDate.of(2021, 02, 02));
        
        Jogo j1 = new Jogo(1, 2, 2, EOperator.soma, 4, 4, 10);
        Jogo j2 = new Jogo(2, 8, 2, EOperator.subtracao, 6, 6, 10);
        Jogo j3 = new Jogo(3, 1, 1, EOperator.soma, 2, 3, 10);
        
        List<Jogo> jl = new ArrayList<>();
        jl.add(j1);
        jl.add(j2);
        jl.add(j3);
 
        LocalDateTime inicio = LocalDateTime.of(2021, 10, 21, 00, 00);
        long segundos = inicio.until(LocalDateTime.now(), ChronoUnit.SECONDS);
		
        
        Partida p1 = new Partida(1, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        p1.setUsuario(user);
        p1.setJogoList(jl);
        p1.setTempo(segundos);
        
        Partida Esperado = p1;
	
        Mockito.when(pDao.findByIdAndUsuarioId(Mockito.any(Integer.class), Mockito.any(Integer.class))).thenReturn(p1);
        Mockito.when(pDao.save(Mockito.any())).thenReturn(p1);
                
        //execução
        Partida Obtido = pServ.FinalizaPartida(1, 1, inicio);
        
        //verificação
        assertEquals(Esperado, Obtido);
    
            
    }
    
    @Test
    public void testFinalizaPartidaAcertandoTudo() throws Exception {
        //cenario 
        Usuario user = new Usuario(1, "zezin", "Zezin1", "Ze@Ze", "Zezin#1", "Cidade1", LocalDate.of(2021, 02, 02));
        
        Jogo j1 = new Jogo(1, 2, 2, EOperator.soma, 4, 4, 10);
        Jogo j2 = new Jogo(2, 8, 2, EOperator.subtracao, 6, 6, 10);
        Jogo j3 = new Jogo(3, 1, 1, EOperator.soma, 2, 2, 10);
        
        List<Jogo> jl = new ArrayList<>();
        jl.add(j1);
        jl.add(j2);
        jl.add(j3);
 
        LocalDateTime inicio = LocalDateTime.of(2021, 10, 21, 00, 00);
        long segundos = inicio.until(LocalDateTime.now(), ChronoUnit.SECONDS);
	
        Partida p1 = new Partida(1, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        p1.setUsuario(user);
        p1.setJogoList(jl);
        p1.setTempo(segundos);
        p1.setBonificacao(p1.getBonificacao() * 2);
        
        Partida Esperado = p1;
	
        Mockito.when(pDao.findByIdAndUsuarioId(Mockito.any(Integer.class), Mockito.any(Integer.class))).thenReturn(p1);
        Mockito.when(pDao.save(Mockito.any())).thenReturn(p1);
                
        //execução
        Partida Obtido = pServ.FinalizaPartida(1, 1, inicio);
        
        //verificação
        assertEquals(Esperado, Obtido);
    
            
    }
    
    @Test
    public void testFinalizaPartidaComPartidaNula() {
        //cenario 
        LocalDateTime inicio = LocalDateTime.of(2021, 10, 21, 00, 00);
        Mockito.when(pDao.findByIdAndUsuarioId(Mockito.any(Integer.class), Mockito.any(Integer.class))).thenReturn(null);
                
        Partida Obtido;
        try {
            //execução
            Obtido = pServ.FinalizaPartida(1, 1, inicio);
            fail("Erro: Finalizou a Partida!!!");
        } catch (Exception ex) {
            //verificação
            assertTrue(true);
        }
            
    }


    @Test
    public void testgetPartida() throws Exception {
        //cenario 
        Usuario user = new Usuario(1, "zezin", "Zezin1", "Ze@Ze", "Zezin#1", "Cidade1", LocalDate.of(2021, 02, 02));
        
        Jogo j1 = new Jogo(1, 2, 2, EOperator.soma, 4, 4, 10);
        Jogo j2 = new Jogo(2, 8, 2, EOperator.subtracao, 6, 6, 10);
        Jogo j3 = new Jogo(3, 1, 1, EOperator.soma, 2, 2, 10);
        
        List<Jogo> jl = new ArrayList<>();
        jl.add(j1);
        jl.add(j2);
        jl.add(j3);
 
        Partida p1 = new Partida(1, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        p1.setUsuario(user);
        p1.setJogoList(jl);
        
        Partida Esperado = p1;
	
        Mockito.when(pDao.findByIdAndUsuarioId(Mockito.any(Integer.class), Mockito.any(Integer.class))).thenReturn(p1);
                
        //execução
        Partida Obtido = pServ.getPartida(1, 1);
        
        //verificação
        assertEquals(Esperado, Obtido);
    
            
    }

    @Test
    public void testgetPartidaPartidaNula() {
        
        //cenario 
        Mockito.when(pDao.findByIdAndUsuarioId(Mockito.any(Integer.class), Mockito.any(Integer.class))).thenReturn(null);
                
        Partida Obtido;
        try {
            //execução
            Obtido = pServ.getPartida(1, 1);
            fail("Erro: Partida nulla Recuperada!!!");
        } catch (Exception ex) {
            //verificação
            assertTrue(true);
        }
            
    }
    
    @Test
    public void testgetRanking() {
        //cenario 
        Usuario user1 = new Usuario(1, "Zezinho", "Zezinho1", "ze@ze", "Zezinho#1", "cidade1", LocalDate.of(2000, 01, 01));
        Usuario user2 = new Usuario(2, "Uguinho", "Uguinho1", "Ugo@Ugo", "Uguinho#1", "cidade2", LocalDate.of(2000, 01, 01));
        
        Partida partida = new Partida(1, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        partida.setUsuario(user1);
        
        List<RankingDTO> Esperado = new ArrayList<>();
        Esperado.add(new RankingDTO(user1.getId(), user1.getNome(), 20, 2, 2));
        Esperado.add(new RankingDTO(user2.getId(), user2.getNome(), 10, 1, 1));
	
        Mockito.when(pDao.getRanking()).thenReturn(Esperado);
                
        //execução
        List<RankingDTO> Obtido = pServ.getRanking();
        
        //verificação
        assertEquals(Esperado, Obtido);
            
    }

    
}












