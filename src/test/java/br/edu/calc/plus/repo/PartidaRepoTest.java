/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.repo;

import br.edu.calc.plus.domain.EOperator;
import br.edu.calc.plus.domain.Jogo;
import br.edu.calc.plus.domain.Partida;
import br.edu.calc.plus.domain.Usuario;
import br.edu.calc.plus.domain.dto.RankingDTO;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

/**
 *
 * @author saulo
 */
@DataJpaTest            
@ActiveProfiles("test")
public class PartidaRepoTest {

    @Autowired
    private UsuarioRepo uDao;
  
    @Autowired
    private PartidaRepo pDao;
    
    @Autowired
    private JogoRepo jDao;
    
    private Usuario u = new Usuario(null, "Zezinho", "Zezinho1", "ze@ze", "Zezinho#1", "cidade1", LocalDate.of(2000, 01, 01));
                                    //id, data, bonificacao,tempo   
    private Partida p = new Partida(null, LocalDateTime.of(2021, 10, 20, 00, 00), 10, 1);
                    //idjogo, valor1, valor2, operador, resultado, resposta, bonus
    private Jogo j1 = new Jogo(null, 2, 2, EOperator.soma, 4, 4, 10);
    private Jogo j2 = new Jogo(null, 4, 2, EOperator.subtracao, 2, 1, 10);
    private Jogo j3 = new Jogo(null, 8, 2, EOperator.subtracao, 6, 2, 10);
    private Jogo j4 = new Jogo(null, 2, 4, EOperator.multiplicacao, 8, 8, 10);
    private Jogo j5 = new Jogo(null, 2, 3, EOperator.multiplicacao, 6, 6, 10);
    
    private Usuario u2 = new Usuario(null, "Uguinho", "Uguinho1", "Ugo@Ugo", "Uguinho#1", "cidade2", LocalDate.of(2000, 01, 01));
    private Partida p2 = new Partida(null, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
                    //idjogo, valor1, valor2, operador, resultado, resposta, bonus
    private Jogo jp1 = new Jogo(null, 2, 2, EOperator.soma, 4, 4, 10);
    private Jogo jp2 = new Jogo(null, 4, 2, EOperator.subtracao, 2, 1, 10);
    private Jogo jp3 = new Jogo(null, 8, 2, EOperator.subtracao, 6, 2, 10);
    private Jogo jp4 = new Jogo(null, 2, 3, EOperator.multiplicacao, 6, 6, 10);
    
    
    public PartidaRepoTest() {
    }
    
    @BeforeEach
    public void setUp() {
        System.out.println("Começou um teste!!!");
  
        Usuario usuario = uDao.save(u);
        p.setUsuario(usuario);
        Partida partida = pDao.save(p);
        
        j1.setPartida(partida);
        j2.setPartida(partida);
        j3.setPartida(partida);
        j4.setPartida(partida);
        j5.setPartida(partida);
        
        jDao.save(j1);
        jDao.save(j2);
        jDao.save(j3);
        jDao.save(j4);
        jDao.save(j5);
        
        Usuario usuario2 = uDao.save(u2);
        p2.setUsuario(usuario2);
        Partida partida2 = pDao.save(p2);
        
        jp1.setPartida(partida2);
        jp2.setPartida(partida2);
        jp3.setPartida(partida2);
        jp4.setPartida(partida2);
        
        jDao.save(jp1);
        jDao.save(jp2);
        jDao.save(jp3);
        jDao.save(jp4);
        
    }
    
    @AfterEach
    public void tearDown() {
        System.out.println("Terminou um teste!!!");
        jDao.deleteAll();
        pDao.deleteAll();
        uDao.deleteAll();
        
    }

    @Test
    public void testTodosBonus() {
        //Cenario
        double Esperado = 20;
        
        //execução
        double Obtido = pDao.getAllBonus();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testBuscarPeloIdUsuario() {
        //Cenario
        Usuario user = uDao.findByNome("Zezinho").get();
        Partida Esperado = p;
        
        //execução
        List<Partida> Obtido = pDao.findByUsuarioId(user.getId());
        
        //verificação
        assertEquals(Esperado, Obtido.get(0));
        
    }
    
    @Test
    public void testBuscarUsuarioCompetil() {
        //Cenario
        Usuario user = uDao.findByNome("Zezinho").get();
        
        Partida partida = new Partida(null, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        partida.setUsuario(user);
        Partida aux = pDao.save(partida);
        
        Jogo jogo = new Jogo(null, 2, 2, EOperator.soma, 4, 4, 10);
        jogo.setPartida(aux);
        jDao.save(jogo);
        
        int Esperado = 2;
        
        //execução
        long Obtido = pDao.getUsuarioCompetil(user.getId(), LocalDateTime.of(2021, 10, 20, 00, 00),
                LocalDateTime.of(2021, 10, 21, 23, 59));
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testBuscarPeloIdeUsuarioId() {
        //Cenario
        Usuario user = uDao.findByNome("Zezinho").get();
        
        Partida partida = new Partida(null, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        partida.setUsuario(user);
        Partida Esperado = pDao.save(partida);
        
        Jogo jogo = new Jogo(null, 2, 2, EOperator.soma, 4, 4, 10);
        jogo.setPartida(Esperado);
        jDao.save(jogo);
        
        //execução
        Partida Obtido = pDao.findByIdAndUsuarioId(Esperado.getId(), user.getId());
        
        //verificação
        assertEquals(Esperado, Obtido);
    
    }
    
    @Test
    public void testRanking() {
        //Cenario
        Usuario user1 = uDao.findByNome("Zezinho").get();
        Usuario user2 = uDao.findByNome("Uguinho").get();
        
        Partida partida = new Partida(null, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        partida.setUsuario(user1);
        Partida aux = pDao.save(partida);
        
        Jogo jogo = new Jogo(null, 2, 2, EOperator.soma, 4, 4, 10);
        jogo.setPartida(aux);
        jDao.save(jogo);
        
        
        List<RankingDTO> Esperado = new ArrayList<>();
        Esperado.add(new RankingDTO(user1.getId(), user1.getNome(), 20, 2, 2));
        Esperado.add(new RankingDTO(user2.getId(), user2.getNome(), 10, 1, 1));
        
        //execução
        List<RankingDTO> Obtido = pDao.getRanking();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    @Transactional
    public void testSalvar() {
        //Cenario
        Usuario user1 = uDao.findByNome("Zezinho").get();
        
        Partida partida = new Partida(null, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        partida.setUsuario(user1);
        Partida aux = pDao.save(partida);
        
        Jogo jogo = new Jogo(null, 2, 2, EOperator.soma, 4, 4, 10);
        jogo.setPartida(aux);
        jDao.save(jogo);
        
        int Esperado = 3;
        
        //execução
        long Obtido = pDao.count();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    @Transactional
    public void testSalvarTodos() {
        //Cenario
        Usuario user1 = uDao.findByNome("Zezinho").get();
        
        Partida partida1 = new Partida(null, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        partida1.setUsuario(user1);
        
        Partida partida2 = new Partida(null, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        partida2.setUsuario(user1);
        
        List<Partida> listP = new ArrayList<>();
        listP.add(partida1);
        listP.add(partida2);
        
        int Esperado = 4;
        
        //execução
        pDao.saveAll(listP);
        long Obtido = pDao.count();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testApagar() {
        //Cenario
        Usuario user = uDao.findByNome("Zezinho").get();
        List<Partida> partida = pDao.findByUsuarioId(user.getId());
        
        int Esperado = 1;
        
        //execução
        jDao.deleteAll();
        pDao.delete(partida.get(0));
        long Obtido = pDao.count();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
           
    }
    
    @Test
    public void testApagarPeloId() {
        //Cenario
        Usuario user = uDao.findByNome("Zezinho").get();
        List<Partida> partida = pDao.findByUsuarioId(user.getId());
        
        int Esperado = 1;
        
        //execução
        jDao.deleteAll();
        pDao.deleteById(partida.get(0).getId());
        long Obtido = pDao.count();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
           
    }
    
    @Test
    public void testApagarTodos() {
        //Cenario
        jDao.deleteAll();
        pDao.deleteAll();   
        
        List<Partida> Esperado = new ArrayList<>();
        
        //execução
        List<Partida> Obtido = pDao.findAll();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
           
    }
    
    @Test
    public void testBuscarPeloId() {
        //Cenario
        Usuario user = uDao.findByNome("Zezinho").get();
        List<Partida> Esperado = pDao.findByUsuarioId(user.getId());
        
        //execução
        Partida Obtido = pDao.findById(Esperado.get(0).getId()).get();
        
        //verificação
        assertEquals(Esperado.get(0), Obtido);
        
           
    }
    
    @Test
    public void testBuscarTodos() {
        //Cenario
        int Esperado = 2;
        
        //execução
        List<Partida> Obtido = pDao.findAll();
        
        //verificação
        assertEquals(Esperado, Obtido.size());
        
           
    }
    
    @Test
    public void testContar() {
        //Cenario
        int Esperado = 2;
        
        //execução
        long Obtido = pDao.count();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testTest() {
        //Cenario
        int Esperado = 0;
        
        //execução
        jDao.deleteAll();
        pDao.deleteAll();   
        
        long Obtido = pDao.count();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testExistPeloId() {
        //Cenario
        Usuario user = uDao.findByNome("Zezinho").get();
        List<Partida> aux = pDao.findByUsuarioId(user.getId());
        
        boolean Esperado = true;
        
        //execução
        boolean Obtido = pDao.existsById(aux.get(0).getId());
        
        //verificação
        assertEquals(Esperado, Obtido);
        
           
    }
    
}
