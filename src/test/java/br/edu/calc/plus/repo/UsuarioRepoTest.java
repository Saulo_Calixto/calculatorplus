/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.repo;

import br.edu.calc.plus.domain.EOperator;
import br.edu.calc.plus.domain.Usuario;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

/**
 *
 * @author saulo
 */
@DataJpaTest            
@ActiveProfiles("test")
public class UsuarioRepoTest {
    
    @Autowired
    private UsuarioRepo uDao;
//  id, nome, login, email, senha, cidade, dataNascimento
    private Usuario u1 = new Usuario(null, "Zezinho", "Zezinho1", "ze@ze", "Zezinho#1", "cidade1", LocalDate.of(2000, 01, 01));
    private Usuario u2 = new Usuario(null, "Uguinho1", "Uguinho1", "ugo@ugo1", "Uguinho#1", "cidade2", LocalDate.of(2000, 02, 02));
        
    
    public UsuarioRepoTest() {
    }

    @BeforeEach
    @Transactional
    public void setUp() {
        System.out.println("Começou um teste!!!");
        uDao.save(u1);
        uDao.save(u2);
    }
    
    @AfterEach
    public void tearDown() {
        System.out.println("Terminou um teste!!!");
        uDao.deleteAll();
    }

    @Test
    @Transactional
    public void testSalvarNovoUsuario() {
        //cenario
        Usuario UserEsperado = new Usuario(null, "Matheus", "Matheus2", "mat@mat", "Matheus#2", "cidadeX", LocalDate.of(2000, 02, 02));
        uDao.save(UserEsperado);
        
        //execução
        Usuario UserObtido = uDao.findByNome("Matheus").get();
        
        //visualização
        assertEquals(UserEsperado, UserObtido);
    }
    
    @Test
    @Transactional
    public void testBuscarUsuarioPeloId() {
        //cenario
        Usuario UserEsperado = new Usuario(null, "Matheus", "Matheus2", "mat@mat", "Matheus#2", "cidadeX", LocalDate.of(2000, 02, 02));
        uDao.save(UserEsperado);

        //execução
        Usuario aux = uDao.findByLogin("Matheus2").get();
        Usuario UserObtido = uDao.findById(aux.getId()).get();

        //visualização
        assertEquals(UserEsperado, UserObtido);
    }
    
    @Test
    public void testBuscarUsuarioPeloNome() {
        //cenario
        Usuario UserEsperado = u1;
        
        //execução
        Usuario UserObtido = uDao.findByNome("Zezinho").get();
        
        //visualização
        assertEquals(UserEsperado, UserObtido);
    }
    
    @Test
    public void testBuscarUsuarioPeloLogin() {
        //cenario
        Usuario UserEsperado = u2;
        
        //execução
        Usuario UserObtido = uDao.findByLogin("Uguinho1").get();
        
        //visualização
        assertEquals(UserEsperado, UserObtido);
    }
    
    @Test
    @Transactional
    public void testBuscarTodos() {
        //cenario
        Usuario u3 = new Usuario(null, "Uguinho3", "Uguinho3", "ugo@ugo3", "Uguinho#3", "cidade3", LocalDate.of(2000, 02, 02));
        Usuario u4 = new Usuario(null, "Uguinho4", "Uguinho4", "ugo@ugo4", "Uguinho#4", "cidade4", LocalDate.of(2000, 02, 02));
        uDao.save(u3);
        uDao.save(u4);
        
        int Esperado = 4;
        
        //execução
        ArrayList<Usuario> Obtido = (ArrayList<Usuario>) uDao.findAll();
        
        //visualização
        assertEquals(Esperado, Obtido.size());
    }
    
    @Test
    public void testContar() {
        //cenario
        int Esperado = 2;
        
        //execução
        int Obtido = (int) uDao.count();
        
        //visualização
        assertEquals(Esperado, Obtido);
    }
    
    @Test
    public void testExistePeloId() {
        //cenario
        Usuario User = new Usuario(null, "Matheus", "Matheus2", "mat@mat", "Matheus#2", "cidadeX", LocalDate.of(2000, 02, 02));
        uDao.save(User);
        boolean Esperado = true;
        
        //execução
        Usuario aux = uDao.findByLogin("Matheus2").get();
        boolean Obtido = uDao.existsById(aux.getId());

        //visualização
        assertEquals(Esperado, Obtido);
    }
    
    @Test
    @Transactional
    public void testSalvarTodos() {
        //cenario
        Usuario u3 = new Usuario(null, "Uguinho3", "Uguinho3", "ugo@ugo3", "Uguinho#3", "cidade3", LocalDate.of(2000, 02, 02));
        Usuario u4 = new Usuario(null, "Uguinho4", "Uguinho4", "ugo@ugo4", "Uguinho#4", "cidade4", LocalDate.of(2000, 02, 02));
     
        List<Usuario> lista = new ArrayList<>();
        lista.add(u3);
        lista.add(u4);
        uDao.saveAll(lista);
        int Esperado = 4;
        
        //execução
        int Obtido = (int) uDao.count();
        
        //visualização
        assertEquals(Esperado, Obtido);
    }

    @Test
    @Transactional
    public void testBuscarTodosPeloId() {
        //cenario
        Usuario u4 = new Usuario(null, "Uguinho", "Uguinho4", "ugo@ugo4", "Uguinho#4", "cidade4", LocalDate.of(2000, 02, 02));
        Usuario u5 = new Usuario(null, "Uguinho", "Uguinho5", "ugo@ugo5", "Uguinho#5", "cidade5", LocalDate.of(2000, 02, 02));
        Usuario u6 = new Usuario(null, "Uguinho", "Uguinho6", "ugo@ugo6", "Uguinho#6", "cidade6", LocalDate.of(2000, 02, 02));
        
        uDao.save(u4);
        uDao.save(u5);
        uDao.save(u6);
        
        Usuario aux1 = uDao.findByLogin("Uguinho4").get();
        Usuario aux2 = uDao.findByLogin("Uguinho5").get();
        Usuario aux3 = uDao.findByLogin("Uguinho6").get();
        
        List<Integer> listAux = new ArrayList<>();
        listAux.add(aux1.getId());
        listAux.add(aux2.getId());
        listAux.add(aux3.getId());
        
        List<Usuario> Esperado = new ArrayList<>();
        Esperado.add(aux1);
        Esperado.add(aux2);
        Esperado.add(aux3);
        
        //execução
        List<Usuario> Obtido = uDao.findAllById(listAux);
                
        //visualização
        assertEquals(Esperado, Obtido);
    }
    
    @Test
    public void testApagarPeloId() {
        //cenario
        Usuario User = new Usuario(null, "Matheus", "Matheus2", "mat@mat", "Matheus#2", "cidadeX", LocalDate.of(2000, 02, 02));
        uDao.save(User);
        
        Usuario aux = uDao.findByLogin("Matheus2").get();
        uDao.deleteById(aux.getId());
        
        try {
            //execução
            Usuario Obtido = uDao.findById(aux.getId()).get();
            fail("Erro - Ambos os valores são 0");//errado
        } catch (Exception e) {
            //verificação
            assertTrue(true);//certo
        }
        
    }
    
    @Test
    public void testApagarUsuario() {
        //cenario
        Usuario User = new Usuario(null, "Matheus", "Matheus2", "mat@mat", "Matheus#2", "cidadeX", LocalDate.of(2000, 02, 02));
        uDao.save(User);
        
        Usuario aux = uDao.findByLogin("Matheus2").get();
        uDao.delete(aux);
        
        try {
            //execução
            Usuario Obtido = uDao.findById(aux.getId()).get();
            fail("Erro - Ambos os valores são 0");//errado
        } catch (Exception e) {
            //verificação
            assertTrue(true);//certo
        }
        
    }
    
    @Test
    public void testApagarTodos() {
        //cenario
        Usuario User = new Usuario(null, "Matheus", "Matheus2", "mat@mat", "Matheus#2", "cidadeX", LocalDate.of(2000, 02, 02));
        uDao.save(User);
        uDao.deleteAll();
        ArrayList<Usuario> Esperado = new ArrayList<>();
        
        //Execução
        ArrayList<Usuario> Obtido = (ArrayList<Usuario>) uDao.findAll();
        
        //Verificação
        assertEquals(Esperado, Obtido);
    }
    
}



