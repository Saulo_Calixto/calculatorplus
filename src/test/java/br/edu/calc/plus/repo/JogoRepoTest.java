/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.repo;

import br.edu.calc.plus.domain.EOperator;
import br.edu.calc.plus.domain.Jogo;
import br.edu.calc.plus.domain.Partida;
import br.edu.calc.plus.domain.Usuario;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

/**
 *
 * @author saulo
 */
@DataJpaTest            
@ActiveProfiles("test")
public class JogoRepoTest {
    
    @Autowired
    private UsuarioRepo uDao;
  
    @Autowired
    private PartidaRepo pDao;
    
    @Autowired
    private JogoRepo jDao;
    
    private Usuario u = new Usuario(null, "Zezinho", "Zezinho1", "ze@ze", "Zezinho#1", "cidade1", LocalDate.of(2000, 01, 01));
    private Partida p = new Partida(null, LocalDateTime.of(2021, 10, 20, 00, 00), 2, 1);
                    //idjogo, valor1, valor2, operador, resultado, resposta, bonus
    private Jogo j1 = new Jogo(null, 2, 2, EOperator.soma, 4, 4, 10);
    private Jogo j2 = new Jogo(null, 4, 2, EOperator.subtracao, 2, 1, 10);
    private Jogo j3 = new Jogo(null, 8, 2, EOperator.subtracao, 6, 2, 10);
    private Jogo j4 = new Jogo(null, 2, 4, EOperator.multiplicacao, 8, 8, 10);
    private Jogo j5 = new Jogo(null, 2, 3, EOperator.multiplicacao, 6, 6, 10);
    
    private Usuario u2 = new Usuario(null, "Uguinho", "Uguinho1", "Ugo@Ugo", "Uguinho#1", "cidade2", LocalDate.of(2000, 01, 01));
    private Partida p2 = new Partida(null, LocalDateTime.of(2021, 10, 21, 00, 00), 2, 1);
                    //idjogo, valor1, valor2, operador, resultado, resposta, bonus
    private Jogo jp1 = new Jogo(null, 2, 2, EOperator.soma, 4, 4, 10);
    private Jogo jp2 = new Jogo(null, 4, 2, EOperator.subtracao, 2, 1, 10);
    private Jogo jp3 = new Jogo(null, 8, 2, EOperator.subtracao, 6, 2, 10);
    private Jogo jp4 = new Jogo(null, 2, 3, EOperator.multiplicacao, 6, 6, 10);
    
    
    public JogoRepoTest() {
        
    }

    @BeforeEach
    @Transactional
    public void setUp() {
        System.out.println("Começou um teste!!!");
  
        Usuario usuario = uDao.save(u);
        p.setUsuario(usuario);
        Partida partida = pDao.save(p);
        
        j1.setPartida(partida);
        j2.setPartida(partida);
        j3.setPartida(partida);
        j4.setPartida(partida);
        j5.setPartida(partida);
        
        
        jDao.save(j1);
        jDao.save(j2);
        jDao.save(j3);
        jDao.save(j4);
        jDao.save(j5);
        
        Usuario usuario2 = uDao.save(u2);
        p2.setUsuario(usuario2);
        Partida partida2 = pDao.save(p2);
        
        jp1.setPartida(partida2);
        jp2.setPartida(partida2);
        jp3.setPartida(partida2);
        jp4.setPartida(partida2);
        
        jDao.save(jp1);
        jDao.save(jp2);
        jDao.save(jp3);
        jDao.save(jp4);
    }
    
    @AfterEach
    public void tearDown() {
        System.out.println("Terminou um teste!!!");
        jDao.deleteAll();
        pDao.deleteAll();
        uDao.deleteAll();
        
    }
  
    @Test
    public void testPegarTodosErros() {
        //cenario
        int Esperado = 4;
        
        //execução
        long Obtido = jDao.getAllErros();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testPegarTodosAcertos() {
        //cenario
        int Esperado = 5;
        
        //execução
        long Obtido = jDao.getAllAcertos();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testPegarAcertosDia() {
        //cenario
        int Esperado = 3;
        
        //execução
        long Obtido = jDao.getAcertosDia(LocalDateTime.of(2021, 10, 20, 00, 00),
                LocalDateTime.of(2021, 10, 20, 23, 59));
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testPegarErrosDia() {
        //cenario
        int Esperado = 2;
        
        //execução
        long Obtido = jDao.getErrosDia(LocalDateTime.of(2021, 10, 20, 00, 00),
                LocalDateTime.of(2021, 10, 20, 23, 59));
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }

    @Test
    public void testAcertosUsuario() {
        //cenario
        Usuario user = uDao.findByNome("Zezinho").get();
        int Esperado = 3;
        
        //execução
        long Obtido = jDao.getAllAcertosUser(user.getId());
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    @Transactional
    public void testSalvar() {
        //cenario
        Usuario user = uDao.findByNome("Zezinho").get();
        List<Partida> partida = pDao.findByUsuarioId(user.getId());
        
        Jogo Esperado = new Jogo(null, 4, 2, EOperator.soma, 6, 6, 10);
        Esperado.setPartida(partida.get(0));
        
        //execução
        Jogo Obtido = jDao.save(Esperado);
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    @Transactional
    public void testSalvarTodos() {
        //cenario
        Usuario user = uDao.findByNome("Zezinho").get();
        List<Partida> partida = pDao.findByUsuarioId(user.getId());
        
        Jogo jogo1 = new Jogo(null, 4, 2, EOperator.soma, 6, 6, 10);
        jogo1.setPartida(partida.get(0));
        Jogo jogo2 = new Jogo(null, 4, 2, EOperator.soma, 6, 6, 10);
        jogo2.setPartida(partida.get(0));
        
        List<Jogo> listJ = new ArrayList<>();
        listJ.add(jogo1);
        listJ.add(jogo2);
        
        int Esperado = 11;
        
        //execução
        jDao.saveAll(listJ);
        int Obtido = (int) jDao.count();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }

    
    @Test
    @Transactional
    public void testApagar() {
        //cenario
        Usuario user = uDao.findByNome("Zezinho").get();
        List<Partida> partida = pDao.findByUsuarioId(user.getId());
        
        Jogo j = new Jogo(null, 4, 2, EOperator.soma, 6, 6, 10);
        j.setPartida(partida.get(0));
        Jogo j1 = jDao.save(j);
        jDao.delete(j1);
        
        try {
            //execução
            Jogo Obtido = jDao.findById(j1.getIdjogo()).get();
            fail("Erro - Ambos os valores são 0");//errado
        } catch (Exception e) {
            //verificação
            assertTrue(true);//certo
        }
    }

    @Test
    @Transactional
    public void testApagarPeloId() {
        //cenario
        Usuario user = uDao.findByNome("Zezinho").get();
        List<Partida> partida = pDao.findByUsuarioId(user.getId());
        
        Jogo j = new Jogo(null, 4, 2, EOperator.soma, 6, 6, 10);
        j.setPartida(partida.get(0));
        Jogo j1 = jDao.save(j);
        jDao.deleteById(j1.getIdjogo());
        
        try {
            //execução
            Jogo Obtido = jDao.findById(j1.getIdjogo()).get();
            fail("Erro - Ambos os valores são 0");//errado
        } catch (Exception e) {
            //verificação
            assertTrue(true);//certo
        }
    }
    
    @Test
    @Transactional
    public void testApagarTodos() {
        //cenario
        Usuario user = uDao.findByNome("Zezinho").get();
        List<Partida> partida = pDao.findByUsuarioId(user.getId());
        
        Jogo j = new Jogo(null, 4, 2, EOperator.soma, 6, 6, 10);
        j.setPartida(partida.get(0));
        Jogo j1 = jDao.save(j);
        jDao.deleteAll();
        
        ArrayList<Jogo> Esperado = new ArrayList<>();

        //execução
        ArrayList<Jogo> Obtido = (ArrayList<Jogo>) jDao.findAll();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    
    @Test
    @Transactional
    public void testBuscarPeloId() {
        //cenario
        Usuario user = uDao.findByNome("Zezinho").get();
        List<Partida> partida = pDao.findByUsuarioId(user.getId());
        
        Jogo Esperado = new Jogo(null, 4, 2, EOperator.soma, 6, 6, 10);
        Esperado.setPartida(partida.get(0));
        Jogo j1 = jDao.save(Esperado);
        
        //execução
        Jogo Obtido = jDao.findById(j1.getIdjogo()).get();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testBuscarTodos() {
        //cenario
        int Esperado = 9;
        
        //execução
        ArrayList<Jogo> Obtido = (ArrayList<Jogo>) jDao.findAll();

        //verificação
        assertEquals(Esperado, Obtido.size());
        
    }
    
    @Test
    public void testContar() {
        //cenario
        int Esperado = 9;
        
        //execução
        long Obtido = jDao.count();

        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    @Transactional
    public void testExistePeloId() {
        //cenario
        Usuario user = uDao.findByNome("Zezinho").get();
        List<Partida> partida = pDao.findByUsuarioId(user.getId());
        
        Jogo jogo = new Jogo(null, 4, 2, EOperator.soma, 6, 6, 10);
        jogo.setPartida(partida.get(0));
        Jogo j1 = jDao.save(jogo);
        boolean Esperado = true;
        
        //execução
        boolean Obtido = jDao.existsById(j1.getIdjogo());
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
}
