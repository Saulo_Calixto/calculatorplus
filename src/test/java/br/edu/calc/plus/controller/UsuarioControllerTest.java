/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.controller;

import br.edu.calc.plus.domain.Usuario;
import br.edu.calc.plus.repo.UsuarioRepo;
import java.time.LocalDate;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author saulo
 */
@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
public class UsuarioControllerTest {

    @Autowired
    private MockMvc request;
    
    @Autowired
    private UsuarioRepo uDao;
    
    public UsuarioControllerTest() {
    }
    
    @BeforeEach
    public void setUp() {
        Usuario u1 = new Usuario(null, "Zezinho", "Zezinho1", "ze@ze", "Zezinho#1", "cidade1", LocalDate.of(2000, 01, 01));
        u1 = uDao.save(u1);
        
        System.out.println("Criou o usuário :: "+u1.getId());
    }
    
    @AfterEach
    public void tearDown() {
        uDao.deleteAll();
    }

    @Test
    public void testCadastro() throws Exception {
        //Execução
        ResultActions r = request.perform( MockMvcRequestBuilders.get("/user") );
        
        //verificação
        r.andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.view().name("cadastro"))
        .andDo(MockMvcResultHandlers.print());
        
    }
    
    @Test
    public void testSaveUser() throws Exception {
        //Execução
        ResultActions r = request.perform( MockMvcRequestBuilders.post("/user")
        .with(SecurityMockMvcRequestPostProcessors.csrf())
        .param("nome", "Saulo")
        .param("login", "Saulo1")
        .param("senha", "Saulo#1")
        .param("email", "Saulo@Saulo")
        .param("cidade", "cidade1")
        .param("nascimento", "2001-03-13")); 
        
        //verificação
        r.andExpect(MockMvcResultMatchers.status().isFound())
        .andExpect(MockMvcResultMatchers.redirectedUrl("/home"))
        .andDo(MockMvcResultHandlers.print());
        
    }
    
}
