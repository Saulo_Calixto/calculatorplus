/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.controller;

import br.edu.calc.plus.config.security.user.UserLogado;
import br.edu.calc.plus.domain.EOperator;
import br.edu.calc.plus.domain.Jogo;
import br.edu.calc.plus.domain.Partida;
import br.edu.calc.plus.domain.Usuario;
import br.edu.calc.plus.repo.JogoRepo;
import br.edu.calc.plus.repo.PartidaRepo;
import br.edu.calc.plus.repo.UsuarioRepo;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author saulo
 */
@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
public class RankingControllerTest {
    
    @Autowired
    private MockMvc request;
    
    @Autowired
    private UsuarioRepo uDao;
    
    @Autowired
    private PartidaRepo pDao;
    
    @Autowired
    private JogoRepo jDao;
    
    private Usuario u = new Usuario(null, "Zezinho", "Zezinho1", "ze@ze", "Zezinho#1", "cidade1", LocalDate.of(2000, 01, 01));
                                    //id, data, bonificacao,tempo   
    private Partida p = new Partida(null, LocalDateTime.of(2021, 10, 20, 00, 00), 10, 1);
                    //idjogo, valor1, valor2, operador, resultado, resposta, bonus
    private Jogo j1 = new Jogo(null, 2, 2, EOperator.soma, 4, 4, 10);
    private Jogo j2 = new Jogo(null, 4, 2, EOperator.subtracao, 2, 1, 10);
    private Jogo j3 = new Jogo(null, 8, 2, EOperator.subtracao, 6, 2, 10);
    private Jogo j4 = new Jogo(null, 2, 4, EOperator.multiplicacao, 8, 8, 10);
    private Jogo j5 = new Jogo(null, 2, 3, EOperator.multiplicacao, 6, 6, 10);
    
    private Usuario u2 = new Usuario(null, "Uguinho", "Uguinho1", "Ugo@Ugo", "Uguinho#1", "cidade2", LocalDate.of(2000, 01, 01));
    private Partida p2 = new Partida(null, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
                    //idjogo, valor1, valor2, operador, resultado, resposta, bonus
    private Jogo jp1 = new Jogo(null, 2, 2, EOperator.soma, 4, 4, 10);
    private Jogo jp2 = new Jogo(null, 4, 2, EOperator.subtracao, 2, 1, 10);
    private Jogo jp3 = new Jogo(null, 8, 2, EOperator.subtracao, 6, 2, 10);
    private Jogo jp4 = new Jogo(null, 2, 3, EOperator.multiplicacao, 6, 6, 10);
    
    
    public RankingControllerTest() {
    }
    
    @BeforeEach
    public void setUp() {
    
        Usuario usuario = uDao.save(u);
        p.setUsuario(usuario);
        Partida partida = pDao.save(p);
        
        j1.setPartida(partida);
        j2.setPartida(partida);
        j3.setPartida(partida);
        j4.setPartida(partida);
        j5.setPartida(partida);
        
        jDao.save(j1);
        jDao.save(j2);
        jDao.save(j3);
        jDao.save(j4);
        jDao.save(j5);
        
        Usuario usuario2 = uDao.save(u2);
        p2.setUsuario(usuario2);
        Partida partida2 = pDao.save(p2);
        
        jp1.setPartida(partida2);
        jp2.setPartida(partida2);
        jp3.setPartida(partida2);
        jp4.setPartida(partida2);
        
        jDao.save(jp1);
        jDao.save(jp2);
        jDao.save(jp3);
        jDao.save(jp4);
        
    }
    
    @AfterEach
    public void tearDown() {
        jDao.deleteAll();
        pDao.deleteAll();
        uDao.deleteAll();
    }

    @Test
    public void testRanking() throws Exception {
        //Cenario
        Usuario useL = uDao.findByNome("Zezinho").get();
        
        Set<GrantedAuthority> grantedAuthorities  = new HashSet<>();//criando o usuário que vai ser guardado na sessão
        grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_CLIENTE"));
        
        UserLogado u = new UserLogado(useL.getId(), useL.getNome(), useL.getEmail(),
                                useL.getLogin(), useL.getSenha(), grantedAuthorities);
        
        //execução
        ResultActions r = request.perform( MockMvcRequestBuilders.get("/ranking")
                .with(SecurityMockMvcRequestPostProcessors.user(u)));
        
        //verificação
        r.andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.view().name("ranking"))
        .andDo(MockMvcResultHandlers.print());
        
    }
    
}
