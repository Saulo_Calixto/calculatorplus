/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.funcional;

import br.edu.calc.plus.domain.Usuario;
import br.edu.calc.plus.repo.UsuarioRepo;
import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;

/**
 *
 * @author saulo
 */
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class FuncionalTest {

    private WebDriver driver;
    private static final String URL1 = "http://localhost:9001/login";
    private static final String URL2 = "http://localhost:9001/user";
    
    @Autowired
    private UsuarioRepo uDao;

    private PasswordEncoder password = new BCryptPasswordEncoder();

    @BeforeEach
    public void setUp()
    {
        Usuario u = new Usuario(null, "Daves Martins", "daves", "daves@daves", password.encode( "123456" ), "JF", 
                LocalDate.now().minusYears(30)); 
        uDao.save(u);
        
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    
        try {
            Thread.sleep(3500);
        } catch (InterruptedException ex) {
            Logger.getLogger(FuncionalTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    @AfterEach
    public void tearDown() {
        uDao.deleteAll();
        driver.quit();
    }
    
    @Test
    public void realizarLoginCorretamente() {
        driver.get(URL1);
        driver.manage().window().setSize(new Dimension(1366, 728));
        driver.findElement(By.id("username")).sendKeys("daves");
        driver.findElement(By.id("password")).sendKeys("123456");
        driver.findElement(By.cssSelector(".btn")).click();
        driver.findElement(By.cssSelector(".navbar > .container-fluid")).click();
        assertThat(driver.findElement(By.cssSelector(".navbar-brand > span")).getText(), is("Bem Vindo, Daves Martins"));
    }
    
    @Test
    public void digitarLoginErrado() {
        driver.get(URL1);
        driver.manage().window().setSize(new Dimension(1366, 728));
        driver.findElement(By.id("username")).sendKeys("david");
        driver.findElement(By.id("password")).sendKeys("$2a$10$eK3UWjYH3qeYpBFFeTqv1.nJbBdFBi6I/rfENRis6.pMm7SKVKblG");
        driver.findElement(By.cssSelector(".btn")).click();
        driver.findElement(By.cssSelector(".alert")).click();
        assertThat(driver.findElement(By.cssSelector(".alert > span")).getText(), is("Login ou senha incorreta"));
    }
    
    @Test
    public void digitarSenhaErrado() {
        driver.get(URL1);
        driver.manage().window().setSize(new Dimension(1366, 728));
        driver.findElement(By.id("username")).sendKeys("daves");
        driver.findElement(By.id("password")).sendKeys("daves123");
        driver.findElement(By.cssSelector(".btn")).click();
        driver.findElement(By.cssSelector(".alert")).click();
        assertThat(driver.findElement(By.cssSelector(".alert > span")).getText(), is("Login ou senha incorreta"));
    }
    
    @Test
    public void clicarNoLoginSemPreecherOsCampos() {
        driver.get(URL1);
        driver.manage().window().setSize(new Dimension(1366, 728));
        driver.findElement(By.cssSelector(".btn")).click();
        WebElement inputElement = driver.findElement(By.name("username"));
        JavascriptExecutor js = (JavascriptExecutor) driver;  
        boolean isRequired = (Boolean) js.executeScript("return arguments[0].required;",inputElement);
        assertThat(isRequired, is(true));
        
    }
    
    @Test
    public void clicarEmCadastrarAntesDePreencherCampos() {
        
        driver.get(URL2);
        driver.manage().window().setSize(new Dimension(1366, 728));
        driver.findElement(By.cssSelector(".btn")).click();
        WebElement inputElement = driver.findElement(By.name("nome"));
        JavascriptExecutor js = (JavascriptExecutor) driver;  
        boolean isRequired = (Boolean) js.executeScript("return arguments[0].required;",inputElement);
        assertThat(isRequired, is(true));
        //assertEquals(isRequired, true);
    }
    
    @Test
    public void cadastrarEmailSemArouba() {
        driver.get(URL2);
        driver.manage().window().setSize(new Dimension(1366, 728));
        driver.findElement(By.id("cpNome")).sendKeys("Aluno");
        driver.findElement(By.id("cpLogin")).sendKeys("Aluno1");
        driver.findElement(By.id("cpSenha")).sendKeys("Aluno#1");
        driver.findElement(By.id("cpEmail")).sendKeys("Aluno123gmail.com");
        driver.findElement(By.id("cpCidade")).sendKeys("Cidade1");
        driver.findElement(By.id("cpData")).click();
        driver.findElement(By.id("cpData")).sendKeys("2001-03-09");
        driver.findElement(By.cssSelector(".btn")).click();
        WebElement inputElement = driver.findElement(By.name("email"));
        JavascriptExecutor js = (JavascriptExecutor) driver;  
        boolean isRequired = (Boolean) js.executeScript("return arguments[0].required;",inputElement);
        assertThat(isRequired, is(true));
        
    }
   
    @Test
    public void cadastrarUmaDataNascimentoFutura() {
        driver.get(URL2);
        driver.manage().window().setSize(new Dimension(1366, 728));
        driver.findElement(By.id("cpNome")).sendKeys("Aluno");
        driver.findElement(By.id("cpLogin")).sendKeys("Aluno1");
        driver.findElement(By.id("cpSenha")).sendKeys("Aluno#1");
        driver.findElement(By.id("cpEmail")).sendKeys("Aluno123@gmail.com");
        driver.findElement(By.id("cpCidade")).sendKeys("Cidade1");
        driver.findElement(By.id("cpData")).click();
        driver.findElement(By.id("cpData")).sendKeys("2022-10-12");
        driver.findElement(By.cssSelector(".btn")).click();
        {
          List<WebElement> elements = driver.findElements(By.cssSelector(".invalid-feedback > span"));
          assert(elements.size() > 0);
        }
    }
   
    @Test
    public void cadastroPassandoAlgumCampoVazio() {
        driver.get(URL2);
        driver.manage().window().setSize(new Dimension(1366, 728));
        driver.findElement(By.id("cpNome")).sendKeys("Aluno");
        driver.findElement(By.id("cpLogin")).sendKeys("Aluno1");
        driver.findElement(By.id("cpSenha")).sendKeys("Aluno#1");
        driver.findElement(By.id("cpCidade")).sendKeys("Cidade1");
        driver.findElement(By.id("cpData")).click();
        driver.findElement(By.id("cpData")).sendKeys("2000-04-12");
        driver.findElement(By.cssSelector(".btn")).click();
        WebElement inputElement = driver.findElement(By.name("email"));
        JavascriptExecutor js = (JavascriptExecutor) driver;  
        boolean isRequired = (Boolean) js.executeScript("return arguments[0].required;",inputElement);
        assertThat(isRequired, is(true));
        
    }
    
    @Test
    public void cadastroUserCorretamente() {
        driver.get(URL2);
        driver.manage().window().setSize(new Dimension(1366, 728));
        driver.findElement(By.id("cpNome")).sendKeys("Aluno1");
        driver.findElement(By.id("cpNome")).sendKeys("Aluno");
        driver.findElement(By.id("cpLogin")).sendKeys("Aluno1");
        driver.findElement(By.id("cpSenha")).sendKeys("Aluno#1");
        driver.findElement(By.id("cpEmail")).sendKeys("Aluno123@gmail.com");
        driver.findElement(By.id("cpCidade")).sendKeys("Cidade1");
        driver.findElement(By.id("cpData")).click();
        driver.findElement(By.id("cpData")).sendKeys("2001-10-08");
        driver.findElement(By.cssSelector(".btn")).click();
        driver.findElement(By.cssSelector("span:nth-child(4)")).click();
        {

          List<WebElement> elements = driver.findElements(By.cssSelector("span:nth-child(4)"));
          assert(elements.size() > 0);
        }
    }
    
    @Test
    public void cadastroPassandoEmailExistente() {
        driver.get(URL2);
        driver.manage().window().setSize(new Dimension(1366, 728));
        driver.findElement(By.id("cpNome")).sendKeys("Aluno");
        driver.findElement(By.id("cpLogin")).sendKeys("Aluno1");
        driver.findElement(By.id("cpSenha")).sendKeys("Aluno#1");
        driver.findElement(By.id("cpEmail")).sendKeys("Aluno123@gmail.com");
        driver.findElement(By.id("cpCidade")).sendKeys("Cidade1");
        driver.findElement(By.id("cpData")).click();
        driver.findElement(By.id("cpData")).sendKeys("2001-03-13");
        driver.findElement(By.cssSelector(".btn")).click();
        driver.findElement(By.cssSelector("li:nth-child(3) p")).click();
        driver.findElement(By.id("cpNome")).click();
        driver.findElement(By.id("cpNome")).sendKeys("Aluno");
        driver.findElement(By.id("cpLogin")).sendKeys("Aluno2");
        driver.findElement(By.id("cpSenha")).sendKeys("Aluno#2");
        driver.findElement(By.id("cpEmail")).sendKeys("Aluno123@gmail.com");
        driver.findElement(By.id("cpCidade")).sendKeys("Cidade2");
        driver.findElement(By.id("cpData")).click();
        driver.findElement(By.id("cpData")).sendKeys("2004-07-08");
        driver.findElement(By.cssSelector(".btn")).click();
        boolean esperado = false;
        assertEquals(esperado, false);
    }
    
    @Test
    public void cadastroPassandoLoginExistente() {
        driver.get(URL2);
        driver.manage().window().setSize(new Dimension(1366, 728));
        driver.findElement(By.id("cpNome")).click();
        driver.findElement(By.id("cpNome")).sendKeys("Aluno");
        driver.findElement(By.id("cpLogin")).sendKeys("Aluno1");
        driver.findElement(By.id("cpSenha")).sendKeys("Aluno#1");
        driver.findElement(By.id("cpEmail")).sendKeys("Aluno123@gmail.com");
        driver.findElement(By.id("cpCidade")).sendKeys("Cidade1");
        driver.findElement(By.id("cpData")).click();
        driver.findElement(By.id("cpData")).sendKeys("2001-03-13");
        driver.findElement(By.cssSelector(".btn")).click();
        driver.findElement(By.cssSelector("li:nth-child(3) p")).click();
        driver.findElement(By.id("cpNome")).click();
        driver.findElement(By.id("cpNome")).sendKeys("Aluno");
        driver.findElement(By.id("cpLogin")).sendKeys("Aluno1");
        driver.findElement(By.id("cpSenha")).sendKeys("Aluno#456");
        driver.findElement(By.id("cpEmail")).sendKeys("Aluno456@gmail.com");
        driver.findElement(By.id("cpCidade")).sendKeys("Cidade2");
        driver.findElement(By.id("cpData")).click();
        driver.findElement(By.id("cpData")).click();
        driver.findElement(By.id("cpData")).sendKeys("2000-02-10");
        driver.findElement(By.cssSelector(".btn")).click();
        boolean esperado = false;
        assertEquals(esperado, false);
    }
    
    @Test
    public void segundoCadastroMesmosValores() {
        driver.get(URL2);
        driver.manage().window().setSize(new Dimension(1366, 728));
        driver.findElement(By.id("cpNome")).sendKeys("Aluno");
        driver.findElement(By.id("cpLogin")).sendKeys("Aluno1");
        driver.findElement(By.id("cpSenha")).sendKeys("Aluno#1");
        driver.findElement(By.id("cpEmail")).sendKeys("Aluno123@gmail.com");
        driver.findElement(By.id("cpCidade")).sendKeys("Cidade1");
        driver.findElement(By.id("cpData")).click();
        driver.findElement(By.id("cpData")).sendKeys("2001-03-13");
        driver.findElement(By.cssSelector(".btn")).click();
        driver.findElement(By.cssSelector("li:nth-child(3) p")).click();
        driver.findElement(By.id("cpNome")).click();
        driver.findElement(By.id("cpNome")).sendKeys("Aluno");
        driver.findElement(By.id("cpLogin")).sendKeys("Aluno1");
        driver.findElement(By.id("cpSenha")).sendKeys("Aluno#1");
        driver.findElement(By.id("cpEmail")).sendKeys("Aluno123@gmail.com");
        driver.findElement(By.id("cpCidade")).sendKeys("Cidade1");
        driver.findElement(By.id("cpData")).click();
        driver.findElement(By.id("cpData")).sendKeys("2001-03-13");
        driver.findElement(By.cssSelector(".btn")).click();
        boolean esperado = false;
        assertEquals(esperado, false);
    }
    
}
