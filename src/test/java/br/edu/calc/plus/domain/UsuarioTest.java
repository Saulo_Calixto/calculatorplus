/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.domain;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author saulo
 */
public class UsuarioTest {
    
    public UsuarioTest() {
    }
    
    //-----------------loginValida--------------------------
    
    @Test
    public void testLoginValido() {
        //cenário
        Usuario u = new Usuario();
        u.setLogin("usuario123");
        boolean esperado = true; 
                
        //execução
        boolean obtido = u.loginValida();
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testLoginComMuitoExtenso() {
        //cenário
        Usuario u = new Usuario();
        u.setLogin("usuarioUserUsuario12345");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.loginValida();
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testLoginComMuitCurto() {
        //cenário
        Usuario u = new Usuario();
        u.setLogin("use");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.loginValida();
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testLoginComEspaco() {
        //cenário
        Usuario u = new Usuario();
        u.setLogin("usuario 123");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.loginValida();
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    //-----------------senhaValida--------------------------
    
    @Test
    public void testSenhaValida() {
        //cenário
        Usuario u = new Usuario();
        u.setSenha("U$uario#123");
        boolean esperado = true; 
                
        //execução
        boolean obtido = u.senhaValida();
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testSenhaSoComCaracteresMinusculos() {
        //cenário
        Usuario u = new Usuario();
        u.setSenha("u$uario#123");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.senhaValida();
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testSenhaSoComCaracteresMaiusculos() {
        //cenário
        Usuario u = new Usuario();
        u.setSenha("U$UARIO#123");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.senhaValida();
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testSenhaSemNumeros() {
        //cenário
        Usuario u = new Usuario();
        u.setSenha("U$uario#");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.senhaValida();
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testSenhaSemCaracterEspecial() {
        //cenário
        Usuario u = new Usuario();
        u.setSenha("Usuario123");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.senhaValida();
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testSenhaSoComNumeros() {
        //cenário
        Usuario u = new Usuario();
        u.setSenha("123456789");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.senhaValida();
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testSenhaSoComLetras() {
        //cenário
        Usuario u = new Usuario();
        u.setSenha("UsuarioUser");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.senhaValida();
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testSenhaSoComCaracterEspecial() {
        //cenário
        Usuario u = new Usuario();
        u.setSenha("###########");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.senhaValida();
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testSenhaSoComCaracterEspecialEnumeros() {
        //cenário
        Usuario u = new Usuario();
        u.setSenha("#######123");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.senhaValida();
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    //-----------------Equals--------------------------
    
    @Test
    public void testEqualsPassandoObjetosIguais() {
        //cenário
        Usuario u1 = new Usuario();
        u1.setId(1);
        
        Usuario u2 = new Usuario();
        u2.setId(1);
       
        boolean esperado = true; 
                
        //execução
        boolean obtido = u1.equals(u2);
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testEqualsPassandoObjetosDeTipoDiferentes() {
        //cenário
        Usuario u = new Usuario();
        Jogo j = new Jogo();
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.equals(j);
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testEqualsPassandoObjetosDoMesmoTipoMasUmDelesComIdNullo() {
        //cenário
        Usuario u1 = new Usuario();
        u1.setId(1);
        
        Usuario u2 = new Usuario();
       
        boolean esperado = false; 
                
        //execução
        boolean obtido = u1.equals(u2);
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }

    @Test
    public void testEqualsPassandoObjetosComIdDiferentes() {
        //cenário
        Usuario u1 = new Usuario();
        u1.setId(1);
        
        Usuario u2 = new Usuario();
        u2.setId(2);
       
        boolean esperado = false; 
                
        //execução
        boolean obtido = u1.equals(u2);
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testEqualsPassandoObjetosComCamposNullos() {
        //cenário
        Usuario u1 = new Usuario();
        
        Usuario u2 = new Usuario();
        
        boolean esperado = true; 
                
        //execução
        boolean obtido = u1.equals(u2);
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
}
