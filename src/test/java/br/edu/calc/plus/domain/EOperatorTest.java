/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.domain;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author saulo
 */
public class EOperatorTest {
    
    public EOperatorTest() {
    }

    //------------------calcular--------------------
    
    @Test
    public void testSoma() {
        //Cenario
        double esperado = 2;
        
        //execução
        double obtido = EOperator.soma.calcular(1, 1);
        
        //Verificação
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testSubtracao() {
        //Cenario
        double esperado = 5;
        
        //execução
        double obtido = EOperator.subtracao.calcular(10, 5);
        
        //Verificação
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testMultiplicacao() {
        //Cenario
        double esperado = 4;
        
        //execução
        double obtido = EOperator.multiplicacao.calcular(2, 2);
        
        //Verificação
        assertEquals(esperado, obtido);
        
    }
 
    @Test
    public void testDivisao() {
        //Cenario
        double esperado = 5;
        
        //execução
        double obtido = EOperator.divisao.calcular(10, 2);
        
        //Verificação
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testSomaPassando2Valores0() {
        //Cenario
        double esperado = 0;
        
        //execução
        double obtido = EOperator.soma.calcular(0, 0);
        
        //Verificação
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testSubtracaoPassando2Valores0() {
        //Cenario
        double esperado = 0;
        
        //execução
        double obtido = EOperator.subtracao.calcular(0, 0);
        
        //Verificação
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testMultiplicacaoPassando2Valores0() {
        //Cenario
        double esperado = 0;
        
        //execução
        double obtido = EOperator.multiplicacao.calcular(0, 0);
        
        //Verificação
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testDivisaoPassando2Valores0() {
        
        try {
            //execução
            double obtido = EOperator.divisao.calcular(0, 0);
            fail("Erro - Ambos os valores são 0");//errado
        } catch (Exception e) {
            //verificação
            assertTrue(true);//certo
        }
        
    }
    
    
}
