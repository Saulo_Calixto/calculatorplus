/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.domain;

import br.edu.calc.plus.domain.dto.JogoListDTO;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author saulo
 */
public class PartidaTest {


    //-----------getAcertos-----------------
    
    @Test
    public void testPegarAcertos() {
        
        //cenario 
        Usuario user = new Usuario(1, "zezin", "Zezin1", "Ze@Ze", "Zezin#1", "Cidade1", LocalDate.of(2021, 02, 02));
        
        Jogo j1 = new Jogo(1, 2, 2, EOperator.soma, 4, 4, 10);
        Jogo j2 = new Jogo(2, 8, 2, EOperator.subtracao, 6, 6, 10);
        Jogo j3 = new Jogo(3, 1, 1, EOperator.soma, 2, 3, 10);
        
        List<Jogo> jl = new ArrayList<>();
        jl.add(j1);
        jl.add(j2);
        jl.add(j3);
        
        Partida p1 = new Partida(1, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        p1.setUsuario(user);
        p1.setJogoList(jl);
        
        int Esperado = 2;
        
//        for (Jogo jogo : p1.getJogoList()) {
//            Esperado += jogo.estaCerto() ? 1 : 0;
//        }
        
        //execução
        int Obtido = p1.getAcertos();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
   
    @Test
    public void testPegarAcertosComListJogosVazio() {
        
        //cenario 
        Usuario user = new Usuario(1, "zezin", "Zezin1", "Ze@Ze", "Zezin#1", "Cidade1", LocalDate.of(2021, 02, 02));
        
        Partida p1 = new Partida(1, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        p1.setUsuario(user);
        
        int Esperado = 0;
        
        try {
            //Execução
            int Obtido = p1.getAcertos();
            fail("Erro: lista vazia!!!");
        } catch (Exception e) {
            assertTrue(true);
        }
        
    }
   
    
    @Test
    public void testPegarErros() {
        
        //cenario 
        Usuario user = new Usuario(1, "zezin", "Zezin1", "Ze@Ze", "Zezin#1", "Cidade1", LocalDate.of(2021, 02, 02));
        
        Jogo j1 = new Jogo(1, 2, 2, EOperator.soma, 4, 4, 10);
        Jogo j2 = new Jogo(2, 8, 2, EOperator.subtracao, 6, 6, 10);
        Jogo j3 = new Jogo(3, 1, 1, EOperator.soma, 2, 3, 10);
        
        List<Jogo> jl = new ArrayList<>();
        jl.add(j1);
        jl.add(j2);
        jl.add(j3);
        
        Partida p1 = new Partida(1, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        p1.setUsuario(user);
        p1.setJogoList(jl);
        
        int Esperado = 1;
        
        //execução
        int Obtido = p1.getErros();
        
        //verificação
        assertEquals(Esperado, Obtido);
        
    }
    
    @Test
    public void testPegarErrosComListJogosVazio() {
        
        //cenario 
        Usuario user = new Usuario(1, "zezin", "Zezin1", "Ze@Ze", "Zezin#1", "Cidade1", LocalDate.of(2021, 02, 02));
        
        Partida p1 = new Partida(1, LocalDateTime.of(2021, 10, 21, 00, 00), 10, 1);
        p1.setUsuario(user);
        
        int Esperado = 0;
        
        //execução
        try {
            int Obtido = p1.getErros();
            fail("Erro: lista vazia!!!");
        } catch (Exception e) {
            assertTrue(true);
        }
        
    }
    
    
    //-----------addBonus----------------------
    
    @Test
    public void testAdicionarBonus() {
        
        //Cenário
        Partida p = new Partida();
        p.setBonificacao(10);
        double esperado = 15;
        
        //execução
        p.addBonus(5);
        double obtido = p.getBonificacao(); 
        
        //verificação
        assertEquals(esperado, obtido);
        
    }
   
    @Test
    public void testAdicionarValorDeBonusPassando0() {
        
        //Cenário
        Partida p = new Partida();
        p.setBonificacao(10);
        double esperado = 10;
        
        //execução
        p.addBonus(0);
        double obtido = p.getBonificacao(); 
        
        //verificação
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testAdicionarBonusPassandoValorNegativo() {
        
        //Cenário
        Partida p = new Partida();
        p.setBonificacao(10);
        double esperado = 10;
        
        //execução
        p.addBonus(-5);
        double obtido = p.getBonificacao(); 
        
        //verificação
        assertEquals(esperado, obtido);
        
    }
    
    //-----------removeBonus----------------------
    
    @Test
    public void testRemoverBonus() {
        
        //Cenário
        Partida p = new Partida();
        p.setBonificacao(10);
        double esperado = 5;
        
        //execução
        p.removeBonus(5);
        double obtido = p.getBonificacao(); 
        
        //verificação
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testRemoverBonusPassandoValorNegativo() {
        
        //Cenário
        Partida p = new Partida();
        p.setBonificacao(10);
        double esperado = 10;
        
        //execução
        p.removeBonus(-5);
        double obtido = p.getBonificacao(); 
        
        //verificação
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testRemoverUmValorDeBonusComBonificaçãoComValor0() {
        
        //Cenário
        Partida p = new Partida();
        p.setBonificacao(0);
        double esperado = 0;
        
        //execução
        p.removeBonus(5);
        double obtido = p.getBonificacao(); 
        
        //verificação
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testRemoverUmValorDeBonusMaiorQueOValorDaBonificação() {
        
        //Cenário
        Partida p = new Partida();
        p.setBonificacao(10);
        double esperado = 0;
        
        //execução
        p.removeBonus(15);
        double obtido = p.getBonificacao(); 
        
        //verificação
        assertEquals(esperado, obtido);
        
    }
    
    //---------------Equals------------------------------------
    
    
    @Test
    public void testEqualsPassandoObjetosIguais() {
        //cenário
        Partida p1 = new Partida();
        p1.setId(1);
        
        Partida p2 = new Partida();
        p2.setId(1);
        
        boolean esperado = true; 
                
        //execução
        boolean obtido = p1.equals(p2);
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testEqualsPassandoObjetosDeTipoDiferentes() {
        //cenário
        Partida p = new Partida();
        Usuario u = new Usuario();
        boolean esperado = false; 
                
        //execução
        boolean obtido = p.equals(u);
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testEqualsPassandoObjetosDoMesmoTipoMasUmDelesComIdNullo() {
        //cenário
        Partida p1 = new Partida();
        p1.setId(1);
        
        Partida p2 = new Partida();
        
        boolean esperado = false; 
                
        //execução
        boolean obtido = p1.equals(p2);
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }

    @Test
    public void testEqualsPassandoObjetosComIdDiferentes() {
        //cenário
        Partida p1 = new Partida();
        p1.setId(1);
        
        Partida p2 = new Partida();
        p2.setId(2);
        
        boolean esperado = false; 
                
        //execução
        boolean obtido = p1.equals(p2);
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testEqualsPassandoObjetosComCamposNullos() {
        //cenário
        Partida p1 = new Partida();
        Partida p2 = new Partida();
        boolean esperado = true; 
                
        //execução
        boolean obtido = p1.equals(p2);
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    
}
