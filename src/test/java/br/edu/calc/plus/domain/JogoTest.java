/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.domain;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author saulo
 */
public class JogoTest {
    
    public JogoTest() {
    }

    //------------estacerto---------------------
    
    @Test
    public void testRespostaIgualAoResultado() {
        //Cenario
        Jogo j = new Jogo();
        j.setResultado(10);
        j.setResposta(10);
        boolean esperado = true;
        
        //execuução
        boolean obtido = j.estaCerto();
        
        //verificação
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testRespostaDiferenteDoResultado() {
        //Cenario
        Jogo j = new Jogo();
        j.setResultado(10);
        j.setResposta(15);
        boolean esperado = false;
        
        //execuução
        boolean obtido = j.estaCerto();
        
        //verificação
        assertEquals(esperado, obtido);
        
    }

    @Test
    public void testNaoPassandoResposta() {
        //Cenario
        Jogo j = new Jogo();
        j.setResultado(10);
        boolean esperado = false;
        
        //execuução
        boolean obtido = j.estaCerto();
        
        //verificação
        assertEquals(esperado, obtido);
        
    }
    
    //-------------isCorrect-------------------------
    
    @Test
    public void testRespostaIgualAoResultado2() {
        //Cenario
        Jogo j = new Jogo();
        j.setResultado(10);
        j.setResposta(10);
        boolean esperado = true;
        
        //execuução
        boolean obtido = j.isCorrect();
        
        //verificação
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testRespostaDiferenteDoResultado2() {
        //Cenario
        Jogo j = new Jogo();
        j.setResultado(10);
        j.setResposta(15);
        boolean esperado = false;
        
        //execuução
        boolean obtido = j.isCorrect();
        
        //verificação
        assertEquals(esperado, obtido);
        
    }

    @Test
    public void testNaoPassandoResposta2() {
        //Cenario
        Jogo j = new Jogo();
        j.setResultado(10);
        boolean esperado = false;
        
        //execuução
        boolean obtido = j.isCorrect();
        
        //verificação
        assertEquals(esperado, obtido);
        
    }
    
    //-------------Equals--------------------------

    
    @Test
    public void testEqualsPassandoObjetosIguais() {
        //cenário
        Jogo j1 = new Jogo();
        j1.setIdjogo(1);
        
        Jogo j2 = new Jogo();
        j2.setIdjogo(1);
        
        boolean esperado = true; 
                
        //execução
        boolean obtido = j1.equals(j2);
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testEqualsPassandoObjetosDeTipoDiferentes() {
        //cenário
        Jogo j = new Jogo();
        Usuario u = new Usuario();
        boolean esperado = false; 
                
        //execução
        boolean obtido = j.equals(u);
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testEqualsPassandoObjetosDoMesmoTipoMasUmDelesComIdNullo() {
        //cenário
        Jogo j1 = new Jogo();
        j1.setIdjogo(1);
        
        Jogo j2 = new Jogo();
        
        boolean esperado = false; 
                
        //execução
        boolean obtido = j1.equals(j2);
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }

    @Test
    public void testEqualsPassandoObjetosComIdDiferentes() {
        //cenário
        Jogo j1 = new Jogo();
        j1.setIdjogo(1);
        
        Jogo j2 = new Jogo();
        j2.setIdjogo(2);
        boolean esperado = false; 
                
        //execução
        boolean obtido = j1.equals(j2);
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
    @Test
    public void testEqualsPassandoObjetosComCamposNullos() {
        //cenário
        Jogo j1 = new Jogo();
        Jogo j2 = new Jogo();
        boolean esperado = true; 
                
        //execução
        boolean obtido = j1.equals(j2);
        
        //verificacção
        assertEquals(esperado, obtido);
        
    }
    
}
